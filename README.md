# atlas-analytics


## About Atlas Analytics

Atlas Analytics provides data collection services to your Decentraland scene. Atlas Analytics gives you access to scene dashboards which are accessible for each scene you have atlas-analytics deployed to. Access to the dashboards can be governed by editing tags in your scene's `scene.json` file, and are natively available to the owner of the land.

These analytics are intended to provide insight into:
    
- user traffic reports
- event reporting and participation (e.g. to compile lists for airdrops or thank-you notes)
- scene loading statistics
- user demographics

To install, follow the instructions at the gitbook here: https://atlas-corporation.gitbook.io/atlas-analytics/

Current version: 0.4.2

